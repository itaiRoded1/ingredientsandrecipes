﻿using System.Web.Mvc;


using IngredientsAndRecipes.Controllers;
using IngredientsAndRecipes.Models.IngredientsAndRecipes;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IngredientsAndRecipes.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
      
        [TestMethod]
        public void IngredientsAndRecipesTest()
        {
            var controller = new HomeController();

            //Call the action we want to test
            var result = controller.IngredientsAndRecipes() as ViewResult;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);

            Assert.IsInstanceOfType(result.Model, typeof (IngredientsAndRecipesViewModel));

            TestInputAndOutputAgainstOurHradCodedValues(result);
        }

        [TestMethod]
        private void TestInputAndOutputAgainstOurHradCodedValues(ViewResult result)
        {
            var ourVieModel = result.Model as IngredientsAndRecipesViewModel;

            Assert.IsNotNull(ourVieModel);

            Assert.IsNotNull(ourVieModel.Ingredients);
            Assert.IsNotNull(ourVieModel.Recipes);
            Assert.IsNotNull(ourVieModel.ComputedRecipeValues);
            
            TestInput(ourVieModel);

            TestOutput(ourVieModel);
        }

        [TestMethod]
        private void TestInput(IngredientsAndRecipesViewModel ourVieModel)
        {
            //Make sure all values match our hard coded input
            Assert.AreEqual(3, ourVieModel.Ingredients.Count);
            Assert.AreEqual(3, ourVieModel.Recipes.Count);
            Assert.AreEqual(3, ourVieModel.ComputedRecipeValues.Count);

            TestIngridients(ourVieModel);

            TestRecipes(ourVieModel);
        }

        [TestMethod]
        private void TestIngridients(IngredientsAndRecipesViewModel ourVieModel)
        {
            Assert.IsNotNull(ourVieModel);
            Assert.IsNotNull(ourVieModel.Ingredients);

            //check Produce hard coded Ingredient (notice ingredient 1 is at position 0)
            int ingridientNumWeTest = 0;
            Assert.AreEqual(3, ourVieModel.Ingredients[ingridientNumWeTest].IngredientItems.Count);
            Assert.AreEqual(IngredientCategory.Produce, ourVieModel.Ingredients[ingridientNumWeTest].Category);

            //check Meat/poultry hard coded Ingredient
            ingridientNumWeTest = 1;
            Assert.AreEqual(2, ourVieModel.Ingredients[ingridientNumWeTest].IngredientItems.Count);
            Assert.AreEqual(IngredientCategory.MeatPoultry, ourVieModel.Ingredients[ingridientNumWeTest].Category);

            //check Pantry hard coded Ingredient
            ingridientNumWeTest = 2;
            Assert.AreEqual(5, ourVieModel.Ingredients[ingridientNumWeTest].IngredientItems.Count);
            Assert.AreEqual(IngredientCategory.Pantry, ourVieModel.Ingredients[ingridientNumWeTest].Category);
        }

        [TestMethod]
        private void TestRecipes(IngredientsAndRecipesViewModel ourVieModel)
        {
            Assert.IsNotNull(ourVieModel);
            Assert.IsNotNull(ourVieModel.Recipes);

            //check recipes hard coded values (notice recipe 1 is at position 0)
            int recipeNumWeTest = 0;
            Assert.AreEqual(5, ourVieModel.Recipes[recipeNumWeTest].RecipeItems.Count);
            Assert.AreEqual(1, ourVieModel.Recipes[recipeNumWeTest].RecipeNum);

            recipeNumWeTest = 1;
            Assert.AreEqual(4, ourVieModel.Recipes[recipeNumWeTest].RecipeItems.Count);
            Assert.AreEqual(2, ourVieModel.Recipes[recipeNumWeTest].RecipeNum);

            recipeNumWeTest = 2;
            Assert.AreEqual(7, ourVieModel.Recipes[recipeNumWeTest].RecipeItems.Count);
            Assert.AreEqual(3, ourVieModel.Recipes[recipeNumWeTest].RecipeNum);
        }

        [TestMethod]
        private void TestOutput(IngredientsAndRecipesViewModel ourVieModel)
        {
            Assert.IsNotNull(ourVieModel);                        
            Assert.IsNotNull(ourVieModel.ComputedRecipeValues);

            //check recipes expected output (notice output for recipe 1 is at position 0)
            int recipeOutputNumWeTest = 0;
            Assert.AreEqual((decimal)0.21, ourVieModel.ComputedRecipeValues[recipeOutputNumWeTest].SalesTaxOnAllNonProduce);
            Assert.AreEqual((decimal)0.11, ourVieModel.ComputedRecipeValues[recipeOutputNumWeTest].WellnessDiscountForOrganicItems);
            Assert.AreEqual((decimal)4.45, ourVieModel.ComputedRecipeValues[recipeOutputNumWeTest].TotalCost);

            recipeOutputNumWeTest = 1;
            Assert.AreEqual((decimal)0.91, ourVieModel.ComputedRecipeValues[recipeOutputNumWeTest].SalesTaxOnAllNonProduce);
            Assert.AreEqual((decimal)0.09, ourVieModel.ComputedRecipeValues[recipeOutputNumWeTest].WellnessDiscountForOrganicItems);
            Assert.AreEqual((decimal)11.84, ourVieModel.ComputedRecipeValues[recipeOutputNumWeTest].TotalCost);

            recipeOutputNumWeTest = 2;
            Assert.AreEqual((decimal)0.42, ourVieModel.ComputedRecipeValues[recipeOutputNumWeTest].SalesTaxOnAllNonProduce);
            Assert.AreEqual((decimal)0.07, ourVieModel.ComputedRecipeValues[recipeOutputNumWeTest].WellnessDiscountForOrganicItems);
            Assert.AreEqual((decimal)8.91, ourVieModel.ComputedRecipeValues[recipeOutputNumWeTest].TotalCost);
        }

    }

}
