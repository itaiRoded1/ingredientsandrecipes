﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web.Mvc;
using IngredientsAndRecipes.Models;
using IngredientsAndRecipes.Models.FluentValidation;

namespace IngredientsAndRecipes.Controllers
{
    public abstract class BaseController : Controller
    {
        protected IPrincipal User { get; private set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            ViewBag.ApmUser = User;
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }

        protected void SetRedirectMessage(String message)
        {
            TempData[ProjectConstants.REDIRECT_MESSAGE_KEY] = message;
        }

        protected void SetRedirectMessage(String key, String message)
        {
            TempData[key] = message;
        }

        protected void AddErrorsToModelState(IEnumerable<ProjectValidationError> errors)
        {
            foreach (ProjectValidationError error in errors)
            {
                ModelState.AddModelError(error.PropertyName, error.Message);
            }
        }
    }
}