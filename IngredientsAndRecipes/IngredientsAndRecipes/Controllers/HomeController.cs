﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using IngredientsAndRecipes.Models.IngredientsAndRecipes;
using IngredientsAndRecipes.Models.IngredientsAndRecipes.RecipeEnteredByUser;
using IngredientsAndRecipes.Models.Tests.ScoreBig;
using IngredientsAndRecipes.Models.Tests.UCLA;
using IngredientsAndRecipes.Services;

namespace IngredientsAndRecipes.Controllers
{
    public class HomeController : BaseController
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return RedirectToAction("ThePurposeOfWebsite");
        }

        [HttpGet]
        public ActionResult RecipeEnteredByUser()
        {
            var model = new RecipeEnteredByUserEditModel();

            return View(model);
        }

        [HttpGet]
        public ActionResult ThePurposeOfWebsite()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RecipeEnteredByUser(RecipeEnteredByUserEditModel model)
        {
            ProjectServiceResult<RecipeEnteredByUserEditModel> result = HomeService.New()
                .UpdateRecipeInSession(model, Session);

            if (!result.IsSuccess)
            {
                AddErrorsToModelState(result.Errors);
            }

            return View(result.Model);
        }


        [HttpGet]
        public ActionResult ComputeRecipeOutput()
        {
            ProjectServiceResult<IngredientsAndRecipesViewModel> result = HomeService.New().ComputeRecipeOutput(Session);

            if (!result.IsSuccess)
            {
                SetRedirectMessage(result.Errors.GetMessages().FirstOrDefault());
            }

            return View(result.Model);
        }


        [HttpGet]
        public ActionResult NewRecipe()
        {
            ProjectServiceResult result = HomeService.New().NewRecipe(Session);

            if (!result.IsSuccess)
            {
                SetRedirectMessage(result.Errors.GetMessages().FirstOrDefault());
            }

            return RedirectToAction("RecipeEnteredByUser");
        }


        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        [HttpGet]
        public ActionResult IngredientsAndRecipes()
        {
            ProjectServiceResult<IngredientsAndRecipesViewModel> result =
                HomeService.New().GetNewIngredientsAndRecipesViewModel();

            if (!result.IsSuccess)
            {
                SetRedirectMessage(result.Errors.GetMessages().FirstOrDefault());
            }

            return View(result.Model);
        }

        public ActionResult Tests()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UCLA1()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UCLA2()
        {
            ProjectServiceResult result = HomeService.New().RemoveDuplicatesFromArray();

            if (!result.IsSuccess)
            {
                SetRedirectMessage(result.Errors.GetMessages().FirstOrDefault());
            }

            SetRedirectMessage(
                "Duplicates have been removed from an array of ints - in one line originalArray.Distinct().ToArray(); ");

            return RedirectToAction("Tests");
        }

        [HttpGet]
        public ActionResult UCLA3()
        {
            var model = new ColumnWithPicturesModel();

            model.Width = 400;

            return View(model);
        }


        [HttpGet]
        public ActionResult ScoreBig()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ScoreBig(FormCollection formCollection)
        {
            String allInputLines = formCollection[0];

            Dictionary<String, List<decimal>> eventsWithPrices = GetInputToDictionary(allInputLines);

            ScoreBigModel model = GetModelFromDic(eventsWithPrices);

            return View(model);

        }

        private decimal GetAverage(List<decimal> allPricesAsList)
        {

            decimal allPrices = allPricesAsList.Sum();

            decimal retVal = allPrices/(allPricesAsList.Count);

            return retVal;

        }

        private Dictionary<String, List<decimal>> GetInputToDictionary(String allInputLines)
        {
            var eventsWithPrices = new Dictionary<string, List<decimal>>();

            //Get lines of input into an list of input lines
            List<String> eventsWithPricesCommaSeperated = Regex.Split(allInputLines, "\r\n").ToList();

            foreach (String eventWithPricesCommaSeperated in eventsWithPricesCommaSeperated)
            {
                //Make sure line of input was not an empty space
                if (!String.IsNullOrWhiteSpace(eventWithPricesCommaSeperated))
                {
                    String[] eventAndPrice = eventWithPricesCommaSeperated.Split(',');

                    String eventName = eventAndPrice[0].Trim(' ');
                    decimal eventPrice = Convert.ToDecimal(eventAndPrice[1]);

                    List<decimal> currentPrices;

                    //Once we have the event name and price we check if we already have some prices for the event
                    if (eventsWithPrices.ContainsKey(eventName)) //if we already have the event in our dictionary 
                    {
                        eventsWithPrices.TryGetValue(eventName, out currentPrices); //get the current event prices

                        if (currentPrices != null) 
                            currentPrices.Add(eventPrice); //and add the price we just got
                    }
                    else //we just see the event for the first time
                    {
                        currentPrices = new List<decimal> {eventPrice}; //create a new prices list for the event

                        eventsWithPrices.Add(eventName, currentPrices); //add to the dictionary 

                    }

                }

            }

            return eventsWithPrices;
        }

        private ScoreBigModel GetModelFromDic(Dictionary<String, List<decimal>> eventsWithPrices)
        {
            var model = new ScoreBigModel();

            //Our view model needs to be sorted by events name 
            //First we'll acquire keys (event names) and sort them. 
            List<string> sortedEvents = eventsWithPrices.Keys.ToList();
            sortedEvents.Sort();

            //Once sorted we find the average price for each event 
            foreach (String eventName in sortedEvents)
            {
                List<decimal> currentPrices;
                eventsWithPrices.TryGetValue(eventName, out currentPrices);
                decimal averagePrice = GetAverage(currentPrices);

                //add we update our view model who will keep a sorted list of events and their average prices 
                model.EventsAndAvegPrices.Add(new KeyValuePair<string, decimal>(eventName, averagePrice));
            }

            return model;

        }

    }

}
