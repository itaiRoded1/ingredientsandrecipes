﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace IngredientsAndRecipes.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString HiddenForModelValue<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            String format = null,
            IDictionary<String, Object> htmlAttributes = null
            )
        {
            if (htmlHelper == null)
            {
                throw new ArgumentNullException("htmlHelper");
            }


            String expressionText = ExpressionHelper.GetExpressionText(expression);

            String fullName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(expressionText);


            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            Object value = metadata.Model;

            String formattedValue = htmlHelper.FormatValue(value, format);


            var tagBuilder = new TagBuilder("input");


            tagBuilder.MergeAttribute("type", HtmlHelper.GetInputTypeString(InputType.Hidden));

            tagBuilder.MergeAttribute("name", htmlHelper.AttributeEncode(fullName));

            tagBuilder.GenerateId(fullName); // sets 'id' attribute value

            tagBuilder.MergeAttribute("value", formattedValue);


            tagBuilder.MergeAttributes(htmlAttributes, false);


            return new MvcHtmlString(tagBuilder.ToString(TagRenderMode.SelfClosing));
        }


        public static String ValueOrDefault(this HtmlHelper htmlHelper, String value, String defaultValue)
        {
            return !String.IsNullOrEmpty(value) ? value : defaultValue;
        }


        public static MvcHtmlString AddClassIfPropertyInError<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            String errorClassName
            )
        {
            String expressionText = ExpressionHelper.GetExpressionText(expression);

            String fullHtmlFieldName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(expressionText);

            ModelState state = htmlHelper.ViewData.ModelState[fullHtmlFieldName];

            if (state == null)
            {
                return MvcHtmlString.Empty;
            }

            if (state.Errors.Count == 0)
            {
                return MvcHtmlString.Empty;
            }

            return new MvcHtmlString(errorClassName);
        }
    }
}