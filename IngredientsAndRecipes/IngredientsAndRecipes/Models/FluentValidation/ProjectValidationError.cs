﻿using System;

namespace IngredientsAndRecipes.Models.FluentValidation {

    public class ProjectValidationError {

        private readonly String _propertyName;
        private readonly String _message;
        
        public static ProjectValidationError New( String message ) {

            return new ProjectValidationError(
                                        ProjectConstants.VALIDATION_FORM_LEVEL_KEY,
                                        message
            );

        }

        public static ProjectValidationError New( String propertyName, String message ) {

            return new ProjectValidationError( propertyName, message );

        }


        private ProjectValidationError( String propertyName, String message ) {

            _propertyName = propertyName;
            _message      = message;

        }

        public String PropertyName {
            get { return _propertyName; }
        }

        public String Message {
            get { return _message; }
        }

    }

}