﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace IngredientsAndRecipes.Models.FluentValidation {

    public sealed class ProjectValidationErrors : IEnumerable<ProjectValidationError> {

        private readonly List<ProjectValidationError> _errors;


        public ProjectValidationErrors() {

            _errors = new List<ProjectValidationError>();

        }


        private List<ProjectValidationError> InternalList {
            get { return _errors; }
        }

        public int Count {
            get { return InternalList.Count; }
        }


        public IEnumerator<ProjectValidationError> GetEnumerator() {

            return InternalList.GetEnumerator();

        }

        IEnumerator IEnumerable.GetEnumerator() {

            return ( (IEnumerable<ProjectValidationError>)this ).GetEnumerator();

        }


        public bool AnyErrors {

            get { return InternalList.Any(); }

        }


        public IEnumerable<String> GetMessages() {

            return InternalList.Select( vr => vr.Message );

        }


        public void Add( String message ) {

            Add( ProjectValidationError.New( message ) );

        }

        public void Add( ProjectValidationError error ) {

            InternalList.Add( error );

        }


        public void AddRange( IEnumerable<ProjectValidationError> errors ) {

            if ( errors == null ) {
                return;
            }

            foreach ( ProjectValidationError error in errors ) {

                Add( error );

            }

        }

        public void AddRange( IEnumerable<String> messages ) {

            if ( messages == null ) {
                return;
            }

            foreach ( String message in messages ) {

                Add( message );

            }

        }

    }

}
