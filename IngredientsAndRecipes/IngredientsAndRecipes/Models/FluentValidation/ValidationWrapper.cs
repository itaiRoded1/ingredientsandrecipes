﻿using FluentValidation;
using FluentValidation.Results;

namespace IngredientsAndRecipes.Models.FluentValidation {

    /*
     * Code adapted from 
     * http://stackoverflow.com/questions/9626472/fluent-validation-rules-subsets-and-nesting
    */

    public static class ValidationWrapper {

        public static bool Validate<T>( AbstractValidator<T> validator, T data, ProjectValidationErrors errors ) {

            ValidationResult result = validator.Validate( data );

            if ( result.IsValid ) {
                return true;
            }

            foreach ( ValidationFailure error in result.Errors ) {

                ProjectValidationError ratingError = ProjectValidationError.New( error.PropertyName, error.ErrorMessage );

                errors.Add( ratingError );

            }

            return false;

        }

    }

}
