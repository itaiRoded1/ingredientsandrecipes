﻿namespace IngredientsAndRecipes.Models.IngredientsAndRecipes
{
    public class ComputedRecipeValues
    {
        public int     RecipeNum                        { get; set; }
        public decimal SalesTaxOnAllNonProduce          { get; set; } //(%8.6 of the total price rounded up to the nearest 7 cents, applies to everything except produce)
        public decimal WellnessDiscountForOrganicItems  { get; set; } //(-%5 of the total price rounded up to the nearest cent, applies only to organic items
        public decimal TotalCost                        { get; set; } //should include the sales tax and the discount)


        public ComputedRecipeValues( int recipeNum, decimal salesTaxOnAllNonProduce, decimal wellnessDiscountForOrganicItems, decimal totalCost)
        {
            RecipeNum = recipeNum;

            SalesTaxOnAllNonProduce = salesTaxOnAllNonProduce;
            WellnessDiscountForOrganicItems = wellnessDiscountForOrganicItems;
            TotalCost = totalCost;            
        }
    }
}