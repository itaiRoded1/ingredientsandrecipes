﻿using System.Collections.Generic;

namespace IngredientsAndRecipes.Models.IngredientsAndRecipes
{
    public class Ingredient
    {
        public IngredientCategory   Category        { get; set; }
        public List<IngredientItem> IngredientItems { get; set; }


        public Ingredient(IngredientCategory cat)
        {
            Category        = cat;
            IngredientItems = new List<IngredientItem>();
        }

        public bool IsProdcue()
        {
            return (this.Category == IngredientCategory.Produce);
        }
        
    }
}