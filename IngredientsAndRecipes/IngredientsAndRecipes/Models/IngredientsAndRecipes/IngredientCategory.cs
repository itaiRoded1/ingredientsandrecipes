﻿namespace IngredientsAndRecipes.Models.IngredientsAndRecipes
{
    public enum IngredientCategory
    {
        Produce, //From Google "Things that have been produced or grown, especially by farming" - THERFORE ORGANIC STUFF 
        MeatPoultry,
        Pantry       
    }
}