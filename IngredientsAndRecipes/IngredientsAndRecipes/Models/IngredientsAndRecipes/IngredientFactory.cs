﻿using System.Collections.Generic;

namespace IngredientsAndRecipes.Models.IngredientsAndRecipes
{
    public class IngredientFactory
    {
        private readonly IngredientCategory   _ingredientCategory;
        private readonly List<IngredientItem> _ingredientItems;

        private IngredientCategory IngredientCategory
        {
            get { return _ingredientCategory; }
        }

        private List<IngredientItem> IngredientItems
        {
            get { return _ingredientItems; }
        }


        public static IngredientFactory New(IngredientCategory ingredientCategory, List<IngredientItem> ingredientItems)
        {
            return new IngredientFactory(ingredientCategory, ingredientItems);
        }

        private IngredientFactory(IngredientCategory ingredientCategory, List<IngredientItem> ingredientItems)
        {
            _ingredientCategory = ingredientCategory;
            _ingredientItems    = ingredientItems;

        }


        public Ingredient Create()
        {
            var retVal = new Ingredient(IngredientCategory);

            retVal.IngredientItems = IngredientItems;

            return retVal;
        }
    }
}