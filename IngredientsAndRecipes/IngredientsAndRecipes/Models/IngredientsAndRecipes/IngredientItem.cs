﻿using System;

namespace IngredientsAndRecipes.Models.IngredientsAndRecipes
{
    public class IngredientItem
    {
        public decimal Qty         { get; set; }
        public String  Description { get; set; }
        public decimal Price       { get; set; }
        public bool    IsOrganic    { get; set; }

        //Forcing creation only by factory
        public IngredientItem() {}
    }
}