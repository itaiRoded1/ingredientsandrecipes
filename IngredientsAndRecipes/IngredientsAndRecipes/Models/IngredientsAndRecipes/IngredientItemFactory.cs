﻿using System;

namespace IngredientsAndRecipes.Models.IngredientsAndRecipes
{
    public class IngredientItemFactory
    {
        private readonly decimal _qty;
        private readonly String  _desc;
        private readonly decimal _price;
        private readonly bool    _isOrganic;

        private decimal Qty
        {
            get { return _qty; }
        }

        private String Desc
        {
            get { return _desc; }
        }

        private decimal Price
        {
            get { return _price; }
        }

        private bool IsOrganic
        {
            get { return _isOrganic; }
        }

        public static IngredientItemFactory New(decimal qty, String desc, decimal price, bool isOrganic)
        {
            return new IngredientItemFactory(qty, desc, price, isOrganic);
        }

        private IngredientItemFactory(decimal qty, String desc, decimal price, bool isOrganic)
        {
            _qty       = qty;
            _desc      = desc;
            _price     = price;
            _isOrganic = isOrganic;
        }


        public IngredientItem Create()
        {
            var retVal = new IngredientItem();

            retVal.Qty = Qty;
            retVal.Description = Desc;
            retVal.Price = Price;
            retVal.IsOrganic = IsOrganic;

            return retVal;

        }

    }

}