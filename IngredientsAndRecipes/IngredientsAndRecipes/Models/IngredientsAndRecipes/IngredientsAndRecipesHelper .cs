﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;


namespace IngredientsAndRecipes.Models.IngredientsAndRecipes
{
    public class IngredientsAndRecipesHelper
    {
        public List<Ingredient> AllPossibleIngredients { get; set; }
        public List<Recipe>     AllRecipes             { get; set; }

        public IngredientsAndRecipesHelper(List<Ingredient> allPossibleIngredients, List<Recipe> allRecipes)
        {
            AllPossibleIngredients = allPossibleIngredients;
            AllRecipes             = allRecipes;
        }


        public ComputedRecipeValues GetComputedValuesForRecipeNum(int recipeNum)
        {
            Recipe recipe = AllRecipes.FirstOrDefault(a => a.RecipeNum == recipeNum);

            Debug.Assert(recipe != null);

            SetPropertiesForEachRecipieItem(recipe);

            decimal salesTax = GetSalesTax(recipe);

            decimal discount = GetDiscount(recipe);

            decimal totaCost = GetTotalCost(recipe, salesTax, discount);

            var computedRecipeValues = new ComputedRecipeValues(recipeNum, salesTax, discount, totaCost);

            return computedRecipeValues;
        }


        private decimal GetDiscount(Recipe recipe)
        {
            Debug.Assert(recipe != null);

            decimal priecForAllOrganicItems = 0;

            foreach (RecipeItem recipeItem in recipe.RecipeItems)
            {
                if (recipeItem.IsOrganic)
                {
                    priecForAllOrganicItems += recipeItem.PriceForTheQty;
                }
            }

            decimal disountPercent = ProjectConstants.DISOUNT_FOR_ORGANIC_PERCENT;

            decimal discount;

            discount = (disountPercent/100)*(priecForAllOrganicItems);

            decimal discountRounded = RoundToCent(discount);

            return discountRounded;
        }


        private decimal RoundToCent(decimal beforeRouding)
        {
            decimal retVal;

            retVal = (int) beforeRouding;

            decimal afterDot = beforeRouding - (int) beforeRouding;

            afterDot = (int) (afterDot*100);

            if (afterDot != 0)
            {
                afterDot = afterDot + 1;
            }

            afterDot = afterDot/100;

            retVal = retVal + afterDot;

            return retVal;
        }


        private decimal GetTotalCost(Recipe recipe, decimal salesTax, decimal discount)
        {
            Debug.Assert(recipe != null);

            decimal totalCost = 0; //Should include the sales tax and the discount

            decimal totalItemsPrices = 0;

            foreach (RecipeItem recipeItem in recipe.RecipeItems)
            {
                totalItemsPrices += recipeItem.PriceForTheQty;
            }

            //We want to round because we want to present only 2 digits after decimal point!
            //I.E (1.7289).ToString("#.##") -> "1.73"
            //I am sure there is a much nicer way of doing so using Math.Round and passing MidpointRounding. something but i am under time constraints 
            String roundedTotalItemPrice = totalItemsPrices.ToString("#.##");
            decimal roundedTotalItemPriceDec = Convert.ToDecimal(roundedTotalItemPrice);                    

            totalCost = roundedTotalItemPriceDec + salesTax - discount;

            Debug.Assert(totalCost != 0);

            return totalCost;
        }


        private decimal GetSalesTax(Recipe recipe)
        {
            Debug.Assert(recipe != null);

            decimal priceAllNonProdcue = GetPriceForAllNonProduce(recipe);

            decimal computedSaleTax;

            decimal saleTax = ProjectConstants.SALES_TAX_PERCENT;

            computedSaleTax = ((saleTax/100)*priceAllNonProdcue);

            decimal computedSaleTaxRounded = RoundedUpToNearestSevenCents(computedSaleTax);

            return computedSaleTaxRounded;
        }


        private decimal RoundedUpToNearestSevenCents(decimal saleTaxBeforeRounding)
        {
            decimal roundsOf7Cents = 0;

            while (roundsOf7Cents < saleTaxBeforeRounding)
            {
                roundsOf7Cents += (decimal) 0.07;
            }

            return roundsOf7Cents;
        }


        private decimal GetPriceForAllNonProduce(Recipe recipe)
        {
            Debug.Assert(recipe != null);

            decimal priceForAllNonProduce = 0;

            foreach (RecipeItem recipeItem in recipe.RecipeItems)
            {
                if (!recipeItem.IsProdcue)
                {
                    priceForAllNonProduce += recipeItem.PriceForTheQty;
                }
            }

            return priceForAllNonProduce;
        }


        private void SetPropertiesForEachRecipieItem(Recipe recipe)
        {
            Debug.Assert(recipe != null);

            foreach (RecipeItem recipeItem in recipe.RecipeItems)
            {
                recipeItem.IsProdcue = IsProdcueItem(recipeItem);
                recipeItem.IsOrganic = IsOrganicItem(recipeItem);
                recipeItem.PriceForTheQty = GetItemPrice(recipeItem);
            }
        }


        private bool IsProdcueItem(RecipeItem recipeItem)
        {
            Debug.Assert(recipeItem != null);

            String itemDesc = recipeItem.ItemDesc;

            foreach (Ingredient ingredient in AllPossibleIngredients)
            {
                if (ingredient.Category == IngredientCategory.Produce)
                {
                    foreach (IngredientItem ingredientItem in ingredient.IngredientItems)
                    {
                        if (ingredientItem.Description.ToLower().Contains(itemDesc.ToLower()))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }


        private bool IsOrganicItem(RecipeItem recipeItem)
        {
            Debug.Assert(recipeItem != null);

            String itemDesc = recipeItem.ItemDesc;

            foreach (Ingredient ingredient in AllPossibleIngredients)
            {
                foreach (IngredientItem ingredientItem in ingredient.IngredientItems)
                {
                    if (ingredientItem.Description.ToLower().Contains(itemDesc.ToLower()))
                    {
                        if (ingredientItem.IsOrganic)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;           
            
        }


        private decimal GetItemPrice(RecipeItem recipeItem)
        {
            Debug.Assert(recipeItem != null);

            String itemDesc = recipeItem.ItemDesc;

            foreach (Ingredient ingredient in AllPossibleIngredients)
            {
                foreach (IngredientItem ingredientItem in ingredient.IngredientItems)
                {
                    if (ingredientItem.Description.ToLower().Contains(itemDesc.ToLower())) //we found how much 1 ingredient of what we need cost
                    {
                        //for 1 it cost
                        decimal priceForOne = ingredientItem.Price;

                        decimal qtyWeNeedPriceFor = recipeItem.ItemQty;

                        decimal priceForQtyWeNeed = qtyWeNeedPriceFor*priceForOne;

                        return priceForQtyWeNeed;
                    }
                }
            }

            return -1;
        }

    }

}
