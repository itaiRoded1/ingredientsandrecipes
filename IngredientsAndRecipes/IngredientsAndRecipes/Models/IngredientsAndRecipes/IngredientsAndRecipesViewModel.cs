﻿using System.Collections.Generic;

namespace IngredientsAndRecipes.Models.IngredientsAndRecipes
{
    public class IngredientsAndRecipesViewModel
    {
        public List<Ingredient> Ingredients { get; set; }
        public List<Recipe>     Recipes     { get; set; }

        public List<ComputedRecipeValues> ComputedRecipeValues { get; set; }

        public IngredientsAndRecipesViewModel()
        {
            Ingredients = new List<Ingredient>();
            Recipes     = new List<Recipe>();

            ComputedRecipeValues = new List<ComputedRecipeValues>();
        }
    }
}