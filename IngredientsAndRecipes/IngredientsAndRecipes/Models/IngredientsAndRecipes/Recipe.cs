﻿using System.Collections.Generic;

namespace IngredientsAndRecipes.Models.IngredientsAndRecipes
{
    public class Recipe
    {
        public int              RecipeNum   { get; set; }
        public List<RecipeItem> RecipeItems { get; set; }

        public Recipe(int recipeNum)
        {
            RecipeNum = recipeNum;

            RecipeItems = new List<RecipeItem>();
        }  
    }
}