﻿using System;

namespace IngredientsAndRecipes.Models.IngredientsAndRecipes.RecipeEnteredByUser
{
    public class RecipeEnteredByUserEditModel
    {
        public decimal Qty            { get; set; }
        public String  IngredientCode { get; set; }

        public RecipeInSession RecipeInSession { get; set; }
    }
}