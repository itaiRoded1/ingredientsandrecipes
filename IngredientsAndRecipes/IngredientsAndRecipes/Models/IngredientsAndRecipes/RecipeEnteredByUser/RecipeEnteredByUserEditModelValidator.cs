﻿using FluentValidation;

namespace IngredientsAndRecipes.Models.IngredientsAndRecipes.RecipeEnteredByUser
{
    public sealed class RecipeEnteredByUserEditModelValidator : AbstractValidator<RecipeEnteredByUserEditModel>
    {
        public RecipeEnteredByUserEditModelValidator()
        {
            RuleFor(m => m.Qty)
                .NotEmpty()
                .WithMessage("Qty Is required")
                .GreaterThan(0)
                .WithMessage("Qty must be greater than 0!");

            RuleFor(m => m.IngredientCode)
                .NotEmpty()
                .WithMessage("Ingredient Is required");
        }
    }
}