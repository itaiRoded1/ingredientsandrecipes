﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IngredientsAndRecipes.Models.IngredientsAndRecipes.RecipeEnteredByUser
{
    public class RecipeInSession
    {
        public List<KeyValuePair<String, String>> ListOfIngredients { get; set; }

        public RecipeInSession()
        {
            ListOfIngredients = new List<KeyValuePair<string, string>>();
        }
    }
}