﻿using System;

namespace IngredientsAndRecipes.Models.IngredientsAndRecipes
{
    public class RecipeItem
    {
        public decimal ItemQty  { get; set; }
        public String  ItemDesc { get; set; }

        public Decimal PriceForTheQty{ get; set; }
        public bool    IsProdcue { get; set; }
        public bool    IsOrganic{ get; set; }

        public RecipeItem(decimal itemQty,String itemDesc)
        {
            ItemQty = itemQty;
            ItemDesc = itemDesc;
        }
    }
}