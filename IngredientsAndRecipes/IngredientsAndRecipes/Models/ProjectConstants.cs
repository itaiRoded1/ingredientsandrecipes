﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IngredientsAndRecipes.Models
{
    public static class ProjectConstants
    {
        public const String VALIDATION_FORM_LEVEL_KEY = "___FORM___";

        public const String REDIRECT_MESSAGE_KEY = "redirect-message-key";

        public const String GARLIC          = "Garlic clove";
        public const String LEMON           = "Lemon";
        public const String OLIVE_OIL       = "Cup olive oil";
        public const String SALT            = "Teaspoons of salt";
        public const String PEPPER          = "Teaspoons of pepper";        
        public const String CORN            = "Cup of corn";
        public const String CHICKEN_BREASTS = "Chicken breasts";
        public const String VINEGAR         = "Cup vinegar";
        public const String BACON           = "Slice of bacon";
        public const String PASTA           = "Ounce of pasta";

        public const decimal SALES_TAX_PERCENT           = (decimal)8.6;
        public const decimal DISOUNT_FOR_ORGANIC_PERCENT = (decimal)5;
    }
}