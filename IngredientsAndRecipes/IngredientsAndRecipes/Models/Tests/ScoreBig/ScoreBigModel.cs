﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IngredientsAndRecipes.Models.Tests.ScoreBig
{
    public class ScoreBigModel
    {
        public  List<KeyValuePair<String, decimal>> EventsAndAvegPrices { get; set; }

        public ScoreBigModel()
        {
            EventsAndAvegPrices = new List<KeyValuePair<string, decimal>>();
        }
    }
}