﻿(function($) {

    $(function() {

        "use strict";

        //operations
        var operations = (function() {
            return {
                execute: function() {
                    return {
                        CalculatePrice: function() {

                            var ajaxSuccess = function(data) {

                                $("#resultsForRecipe").html(data);

                            };

                            var url = $('#link-site-root').val() + "/Home/ComputeRecipeOutput";

                            $.ajax({
                                dataType: "html",
                                url: url,
                                async: false,
                                error: function(xhr, ajaxOptions, thrownError) {
                                    operations.execute().AjaxError(xhr, ajaxOptions, thrownError);
                                },
                                success: ajaxSuccess
                            });

                        },

                        AjaxError: function(xhr, ajaxOptions, thrownError) {

                            console.log("ajax error - the ajax request thrown the following error:");
                            console.log(xhr.responseJSON.exceptionMessage);
                            console.log(xhr.status);
                            console.log(thrownError);

                        }

                    };

                }
            };
        })();

        //register events
        var init = function() {

            $("#GetCurrentReciptPriecBtn").click(function() {

                operations.execute().CalculatePrice();

            });

        };

        init();

    });
})(jQuery);