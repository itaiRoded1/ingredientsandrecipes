﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using IngredientsAndRecipes.Models;
using IngredientsAndRecipes.Models.FluentValidation;
using IngredientsAndRecipes.Models.IngredientsAndRecipes;
using IngredientsAndRecipes.Models.IngredientsAndRecipes.RecipeEnteredByUser;

namespace IngredientsAndRecipes.Services
{
    public class HomeService : ServiceBase
    {
        public static HomeService New()
        {
            return new HomeService();
        }

        private HomeService()
        {
        }

        public ProjectServiceResult<IngredientsAndRecipesViewModel> GetNewIngredientsAndRecipesViewModel()
        {
            var errors = new ProjectValidationErrors();

            IngredientsAndRecipesViewModel model = null;

            try
            {
                //Empty model
                model = new IngredientsAndRecipesViewModel();

                //Populate Fake Data (Later we can have this entered from the UI by the user)
                PopulateIngredients(model);
                PopulateRecipes(model);

                //Use fake data to compute the needed values
                var helper = new IngredientsAndRecipesHelper(model.Ingredients, model.Recipes);

                for (int i = 1; i <= model.Recipes.Count; i++)
                {
                    ComputedRecipeValues recipeComputedValues = helper.GetComputedValuesForRecipeNum(i);
                    model.ComputedRecipeValues.Add(recipeComputedValues);
                }
            }
            catch (Exception e)
            {
                errors.Add(String.Format("Error : {0}", e.Message));
                return Failure(model, errors);
            }

            return Success(model);
        }


        private void PopulateRecipes(IngredientsAndRecipesViewModel model)
        {
            //-----------------------------------------WE can make this part nicer--------------------------
            Recipe recipe = null;
            RecipeItem recipeItem = null;

            //-----------------------------------------Recipe 1----------------------
            recipe = new Recipe(1);

            recipeItem = new RecipeItem(1, ProjectConstants.GARLIC);
            recipe.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem(1, ProjectConstants.LEMON);
            recipe.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem((decimal) FractionToDouble("3/4"), ProjectConstants.OLIVE_OIL);
            recipe.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem((decimal) FractionToDouble("3/4"), ProjectConstants.SALT);
            recipe.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem((decimal) FractionToDouble("1/2"), ProjectConstants.PEPPER);
            recipe.RecipeItems.Add(recipeItem);

            model.Recipes.Add(recipe);

            //-----------------------------------------Recipe 2----------------------
            recipe = new Recipe(2);

            recipeItem = new RecipeItem(1, ProjectConstants.GARLIC);
            recipe.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem(4, ProjectConstants.CHICKEN_BREASTS);
            recipe.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem((decimal) FractionToDouble("1/2"), ProjectConstants.OLIVE_OIL);
            recipe.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem((decimal) FractionToDouble("1/2"), ProjectConstants.VINEGAR);
            recipe.RecipeItems.Add(recipeItem);

            model.Recipes.Add(recipe);

            //-----------------------------------------Recipe 3----------------------
            recipe = new Recipe(3);

            recipeItem = new RecipeItem(1, ProjectConstants.GARLIC);
            recipe.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem(4, ProjectConstants.CORN);
            recipe.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem(4, ProjectConstants.BACON);
            recipe.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem(8, ProjectConstants.PASTA);
            recipe.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem((decimal) FractionToDouble("1/3"), ProjectConstants.OLIVE_OIL);
            recipe.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem((decimal) 1.25, ProjectConstants.SALT);
            recipe.RecipeItems.Add(recipeItem);

            recipeItem = new RecipeItem((decimal) FractionToDouble("3/4"), ProjectConstants.PEPPER);
            recipe.RecipeItems.Add(recipeItem);

            model.Recipes.Add(recipe);
        }

        private void PopulateIngredients(IngredientsAndRecipesViewModel model)
        {
            //-----------------------------------------WE can make this part nicer--------------------------
            Ingredient ingredient = null;
            IngredientItem ingredientItem = null;

            //-----------------------------------------ITEM1----------------------
            ingredient = new Ingredient(IngredientCategory.Produce);

            ingredientItem = IngredientItemFactory.New(1, ProjectConstants.GARLIC, (decimal) 0.67, true).Create();
            ingredient.IngredientItems.Add(ingredientItem);

            ingredientItem = IngredientItemFactory.New(1, ProjectConstants.LEMON, (decimal) 2.03, false).Create();
            ingredient.IngredientItems.Add(ingredientItem);

            ingredientItem = IngredientItemFactory.New(1, ProjectConstants.CORN, (decimal) 0.87, false).Create();
            ingredient.IngredientItems.Add(ingredientItem);

            model.Ingredients.Add(ingredient);


            //-----------------------------------------ITEM2----------------------
            ingredient = new Ingredient(IngredientCategory.MeatPoultry);

            ingredientItem =
                IngredientItemFactory.New(1, ProjectConstants.CHICKEN_BREASTS, (decimal) 2.19, false).Create();
            ingredient.IngredientItems.Add(ingredientItem);

            ingredientItem = IngredientItemFactory.New(1, ProjectConstants.BACON, (decimal) 0.24, false).Create();
            ingredient.IngredientItems.Add(ingredientItem);

            model.Ingredients.Add(ingredient);


            //-----------------------------------------ITEM3----------------------
            ingredient = new Ingredient(IngredientCategory.Pantry);

            ingredientItem = IngredientItemFactory.New(1, ProjectConstants.PASTA, (decimal) 0.31, false).Create();
            ingredient.IngredientItems.Add(ingredientItem);

            ingredientItem = IngredientItemFactory.New(1, ProjectConstants.OLIVE_OIL, (decimal) 1.92, true).Create();
            ingredient.IngredientItems.Add(ingredientItem);

            ingredientItem = IngredientItemFactory.New(1, ProjectConstants.VINEGAR, (decimal) 1.26, false).Create();
            ingredient.IngredientItems.Add(ingredientItem);

            ingredientItem = IngredientItemFactory.New(1, ProjectConstants.SALT, (decimal) 0.16, false).Create();
            ingredient.IngredientItems.Add(ingredientItem);

            ingredientItem = IngredientItemFactory.New(1, ProjectConstants.PEPPER, (decimal) 0.17, false).Create();
            ingredient.IngredientItems.Add(ingredientItem);

            model.Ingredients.Add(ingredient);
        }

        //for ref: http://stackoverflow.com/questions/13903621/convert-a-text-fraction-to-a-decimal
        private double FractionToDouble(string fraction)
        {
            double result;

            if (double.TryParse(fraction, out result))
            {
                return result;
            }

            string[] split = fraction.Split(new[] {' ', '/'});

            if (split.Length == 2 || split.Length == 3)
            {
                int a, b;

                if (int.TryParse(split[0], out a) && int.TryParse(split[1], out b))
                {
                    if (split.Length == 2)
                    {
                        return (double) a/b;
                    }

                    int c;

                    if (int.TryParse(split[2], out c))
                    {
                        return a + (double) b/c;
                    }
                }
            }

            throw new FormatException("Not a valid fraction.");
        }


        internal ProjectServiceResult<IngredientsAndRecipesViewModel> ComputeRecipeOutput(HttpSessionStateBase session)
        {
            var errors = new ProjectValidationErrors();

            IngredientsAndRecipesViewModel model = null;

            try
            {
                //Empty model
                model = new IngredientsAndRecipesViewModel();

                PopulateIngredients(model); //This is a pre defined list of possible recipe ingridents 

                if (session["RecipeInSession"] != null)
                {
                    var recipeInSession = session["RecipeInSession"] as RecipeInSession;

                    PopulateRecipesFromSession(model, recipeInSession);
                }

                //compute the needed values
                var helper = new IngredientsAndRecipesHelper(model.Ingredients, model.Recipes);

                for (int i = 1; i <= model.Recipes.Count; i++)
                {
                    ComputedRecipeValues recipeComputedValues = helper.GetComputedValuesForRecipeNum(i);
                    model.ComputedRecipeValues.Add(recipeComputedValues);
                }
            }
            catch (Exception e)
            {
                errors.Add(String.Format("Error : {0}", e.Message));
                return Failure(model, errors);
            }

            return Success(model);
        }

        private void PopulateRecipesFromSession(IngredientsAndRecipesViewModel model, RecipeInSession recipeInSession)
        {            
            Recipe recipe = null;
            RecipeItem recipeItem = null;
            
            recipe = new Recipe(1);

            List<KeyValuePair<String, String>> listOfIngredients = recipeInSession.ListOfIngredients;

            foreach (var kvp in listOfIngredients)
            {
                decimal qty = Convert.ToDecimal(kvp.Key);
                String itemDesc = kvp.Value;

                recipeItem = new RecipeItem(qty, itemDesc);
                recipe.RecipeItems.Add(recipeItem);
            }

            model.Recipes.Add(recipe);
        }


        public ProjectServiceResult<RecipeEnteredByUserEditModel> UpdateRecipeInSession(RecipeEnteredByUserEditModel model, HttpSessionStateBase session)
        {
            var errors = new ProjectValidationErrors();

            try
            {
                RecipeInSession recipeInSession;

                //Validating edit model 
                if (!ValidationWrapper.Validate(new RecipeEnteredByUserEditModelValidator(), model, errors))
                {
                    if (session["RecipeInSession"] != null)
                    {
                        recipeInSession = session["RecipeInSession"] as RecipeInSession;
                        model.RecipeInSession = recipeInSession;

                    }
                    return Failure(model, errors);
                }

                //The posted model is valid
                var newIngridient = new KeyValuePair<string, string>(model.Qty.ToString(CultureInfo.InvariantCulture), model.IngredientCode);
                
                if (session["RecipeInSession"] != null)
                {
                    recipeInSession = session["RecipeInSession"] as RecipeInSession;

                    if (recipeInSession != null)
                    {
                        recipeInSession.ListOfIngredients.Add(newIngridient);
                    }
                }
                else
                {
                    recipeInSession = new RecipeInSession();

                    recipeInSession.ListOfIngredients.Add(newIngridient);

                }

                session["RecipeInSession"] = recipeInSession;

                model.RecipeInSession = recipeInSession;
            }
            catch (Exception e)
            {
                errors.Add(String.Format("Error : {0}", e.Message));
                return Failure(model, errors);
            }

            return Success(model);
        }


        internal ProjectServiceResult NewRecipe(HttpSessionStateBase session)
        {           
            var errors = new ProjectValidationErrors();

            try
            {
                session["RecipeInSession"] = null;
            }
            catch (Exception e)
            {
                errors.Add(String.Format("Error : {0}", e.Message));
                return Failure(errors);
            }

            return Success();
        }

        public ProjectServiceResult RemoveDuplicatesFromArray()
        {
            var errors = new ProjectValidationErrors();

            try
            {
                int[] originalArray = { 1, 2, 3, 3, 4 };
                int[] arrayWithNoDuplicates = originalArray.Distinct().ToArray();
            }
            catch (Exception e)
            {
                errors.Add(String.Format("Error : {0}", e.Message));
                return Failure(errors);
            }

            return Success();
        }
    }

}