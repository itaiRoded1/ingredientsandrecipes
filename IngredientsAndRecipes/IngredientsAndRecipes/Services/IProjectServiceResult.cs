﻿using IngredientsAndRecipes.Models.FluentValidation;

namespace IngredientsAndRecipes.Services {

      public interface IProjectServiceResult {

        bool                   IsSuccess { get; }
        ProjectValidationErrors  Errors    { get; }

    }

    public interface IProjectServiceResult<T> : IProjectServiceResult  {

        T Model { get; }

    }

}
