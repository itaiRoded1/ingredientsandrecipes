﻿using System;
using System.Diagnostics;
using System.Linq;
using IngredientsAndRecipes.Models.FluentValidation;

namespace IngredientsAndRecipes.Services {

    [DebuggerDisplay( "IsSuccess = {IsSuccess}" )]
    public class ProjectServiceResult : IProjectServiceResult {

        private readonly ProjectValidationErrors _errors;


        public static ProjectServiceResult Success() {

            return new ProjectServiceResult( new ProjectValidationErrors() );

        }

        public static ProjectServiceResult Failure( ProjectValidationErrors errors ) {

            return new ProjectServiceResult( errors );

        }

        public static ProjectServiceResult Failure( String error ) {

            ProjectValidationErrors errors = new ProjectValidationErrors();

            errors.Add( error );

            return new ProjectServiceResult( errors );

        }


        protected ProjectServiceResult( ProjectValidationErrors errors ) {

            _errors = errors;

        }


        public bool IsSuccess {
            get { return !Errors.Any(); }
        }

        public ProjectValidationErrors Errors {
            get { return _errors; }
        }

    }

    //-----------------------------------------------------------------------------------------------------------------------------

    public sealed class ProjectServiceResult<T> : ProjectServiceResult, IProjectServiceResult<T> {

        private readonly T _model;


        public static ProjectServiceResult<T> Success( T model ) {

            return new ProjectServiceResult<T>( model, new ProjectValidationErrors() );

        }

        public static ProjectServiceResult<T> Failure( T model, ProjectValidationErrors errors ) {

            return new ProjectServiceResult<T>( model, errors );

        }


        private ProjectServiceResult(
            T model,
            ProjectValidationErrors errors
            ) : base(
                errors
                ) {

            _model = model;

        }


        public T Model {
            get { return _model; }
        }

    }

}
