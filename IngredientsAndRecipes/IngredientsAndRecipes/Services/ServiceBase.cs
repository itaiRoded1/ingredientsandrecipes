﻿using System;
using System.Web;
using IngredientsAndRecipes.Models.FluentValidation;

namespace IngredientsAndRecipes.Services
{
    public abstract class ServiceBase
    {
        protected T GetObjectThatTalksWithDB<T>() where T : class
        {
            return null;
        }

        protected ProjectServiceResult<T> Success<T>(T model)
        {
            return ProjectServiceResult<T>.Success(model);
        }

        protected ProjectServiceResult Success()
        {
            return ProjectServiceResult.Success();
        }


        protected ProjectServiceResult<T> Failure<T>(T model, ProjectValidationErrors errors)
        {
            return ProjectServiceResult<T>.Failure(model, errors);
        }

        protected ProjectServiceResult Failure(String error)
        {
            return ProjectServiceResult.Failure(error);
        }

        protected ProjectServiceResult Failure(ProjectValidationErrors errors)
        {
            return ProjectServiceResult.Failure(errors);
        }

        protected ProjectServiceResult Failure(ProjectValidationError error)
        {
            var errors = new ProjectValidationErrors();
            errors.Add(error);
            return ProjectServiceResult.Failure(errors);
        }


        public String GetBaseURL()
        {
            String baseUrl = String.Empty;

            if (HttpContext.Current.Request.ApplicationPath != null)
            {
                baseUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority +
                          HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/";
            }
            else
            {
                baseUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority +
                          "/";
            }

            return baseUrl;
        }
    }
}
