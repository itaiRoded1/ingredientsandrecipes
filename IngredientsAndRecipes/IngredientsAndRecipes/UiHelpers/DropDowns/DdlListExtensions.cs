﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using IngredientsAndRecipes.Models;

namespace IngredientsAndRecipes.UiHelpers.DropDowns {

    public static class DdlListExtensions
    {

        public static IEnumerable<SelectListItem> GetAllPOssibleIngredients(this ListItemHelper helper)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem { Text = ProjectConstants.GARLIC, Value = "GARLIC CLOVE" });
            items.Add(new SelectListItem { Text = ProjectConstants.LEMON, Value = "LEMON" });
            items.Add(new SelectListItem { Text = ProjectConstants.OLIVE_OIL, Value = "CUP OLIVE OIL" });
            items.Add(new SelectListItem { Text = ProjectConstants.SALT, Value = "TEASPOONS OF SALT" });
            items.Add(new SelectListItem { Text = ProjectConstants.PEPPER, Value = "TEASPOONS OF PEPPER" });
            items.Add(new SelectListItem { Text = ProjectConstants.CORN, Value = "CUP OF CORN" });
            items.Add(new SelectListItem { Text = ProjectConstants.CHICKEN_BREASTS, Value = "CHICKEN BREASTS" });
            items.Add(new SelectListItem { Text = ProjectConstants.OLIVE_OIL, Value = "CUP OLIVE OIL" });
            items.Add(new SelectListItem { Text = ProjectConstants.VINEGAR, Value = "CUP VINEGAR" });
            items.Add(new SelectListItem { Text = ProjectConstants.BACON, Value = "SLICE OF BACON" });
            items.Add(new SelectListItem { Text = ProjectConstants.PASTA, Value = "OUNCE OF PASTA" });

            items.Insert(0, new SelectListItem { Text = " -- Select Ingredient -- ", Value = String.Empty });

            return items;
        }

    }

}