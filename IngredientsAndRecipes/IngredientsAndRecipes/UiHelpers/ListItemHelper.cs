﻿using System;
using System.Collections;
using System.Web;

namespace IngredientsAndRecipes.UiHelpers {

    public sealed class ListItemHelper {

        private static ListItemHelper _instance;

        private static readonly Object _lock = new Object();


        public static ListItemHelper Instance {

            get {

                if ( _instance == null ) {
                     
                    lock ( _lock ) {

                        if ( _instance == null ) {

                            _instance = new ListItemHelper();

                        }

                    }

                }

                return _instance;

            }

        }

        private ListItemHelper() {}


        private HttpContext HttpContext {
            get { return HttpContext.Current; }
        }

        private IDictionary ContextItems {
            get { return HttpContext.Items; }
        }


        public T GetItem<T>( String key ) where T : class {

            if ( ContextItems.Contains( key ) ) {

                return (T)ContextItems[ key ];

            } else {

                return null;

            }

        }

        public void SetItem( String key, Object value ) {

            ContextItems.Add( key, value );

        }

    }

}