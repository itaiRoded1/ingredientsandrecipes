﻿using System;

namespace IngredientsAndRecipes.UiHelpers {

    public class ListItemModel {

        public String Text  { get; set; }
        public String Value { get; set; }

        public ListItemModel( String text, String value ) {

            Text  = text;
            Value = value;

        }

    }

}